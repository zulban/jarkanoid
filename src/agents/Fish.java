package agents;
import java.util.ArrayList;
import java.util.Arrays;

import behaviours.*;
import generic.Logging;
import generic.Point;
import generic.Utilities;
import simulation.World;

public class Fish extends Agent {
	
	public enum Status{
		FALLING,TUMBLING,SUBMERGED,FLOPPING,RIDING,FROLIC,RESTING,DEAD,TURNING
	}
	public Status status=Status.FROLIC;
	
	public enum Direction{
		LEFT,RIGHT,TURNING
	}
	public Direction direction;
	
	//If the fish decides to queue up several moves like a flop, it puts them in this arraylist.
	private ArrayList<AgentBehaviour> behaviours=new ArrayList<AgentBehaviour>();
	
	private int oxygen=100;
	private int hitPoints=100;
	private int energy=100;
	private final int STAT_MAX=100;
	
	private int finFlapCount;
	private final int FIN_FLAP_TICKS=10;
	
	//how many ticks this fish has been dead.
	private int deathTicks=0;	
	//how many ticks into death until the fish is deleted.
	private final int DEATH_TICK_DELETION=1000;
	
	public int getDeathTicks(){
		return deathTicks;
	}
	
	public boolean isFlappingFin(){
		return finFlapCount<FIN_FLAP_TICKS/2;
	}
	
	private void changeOxygen(int delta){
		oxygen+=delta;
		oxygen=Utilities.clamp(oxygen, 0, STAT_MAX);
	}
	
	private void changeHitPoints(int delta){
		hitPoints+=delta;
		hitPoints=Utilities.clamp(hitPoints, 0, STAT_MAX);
	}
	
	private void changeEnergy(int delta){
		energy+=delta;
		energy=Utilities.clamp(energy, 0, STAT_MAX);
	}
	
	public boolean isAlive(){
		if(hitPoints<=0 || oxygen<=0)
			status = Status.DEAD;
		return status != Status.DEAD;
	}

	public Fish(World world, int x, int y) {
		super(world, x, y);
		
		direction=Utilities.randomInteger(1, 2)==1?Direction.LEFT:Direction.RIGHT;
	}
	
	public boolean isHappy(){
		return hitPoints>90 && oxygen > 70;
	}
	
	public boolean isPanicked(){
		return !isUnderwater() || hitPoints<30 || oxygen < 30;
	}
	
	public boolean wantsRest(){
		return hitPoints<STAT_MAX || oxygen<80 || energy < 50;
	}
	
	private void tickNextBehaviour(){
		if(behaviours.size()==0 || status==Fish.Status.DEAD)
			return;
		
		AgentBehaviour behaviour = behaviours.get(0);
		tickBehaviour(behaviour);
		
		if(behaviours.size() > 0 && behaviour.isFinished())
			behaviours.remove(0);	
	}
	
	private void setAndTickBehaviour(AgentBehaviour behaviour){
		//Like tickBehaviour, except it also clears all other beahviours.
		clearBehaviours();
		behaviours.add(behaviour);
		tickBehaviour(behaviour);
	}
		
	private void tickBehaviour(AgentBehaviour behaviour){
		behaviour.ticksLeft--;
		
		//WaitBehaviour only needs ticksLeft reduced
		
		if(behaviour instanceof MoveBehaviour){
			MoveBehaviour fb = (MoveBehaviour)behaviour;
			int newX=getX()+fb.dx;
			int newY=getY()+fb.dy;
			if(world.isSolid(newX,newY)){
				clearBehaviours();
			}else{
				updatePosition(newX,newY);
			}
		}
	}
	
	private void clearBehaviours(){
		behaviours=new ArrayList<AgentBehaviour>();
	}
	
	public boolean canFall(){
		return !world.isSolid(getX(),getY()-1);
	}
	
	public boolean canTumble(){
		return getTumblePoints().length>0;
	}
	
	public Point[] getTumblePoints(){
		ArrayList<Point> pointsList=new ArrayList<Point>();
		for(int i=-1;i<2;i+=2){
			int x=getX()+i;
			int y=getY()-1;
			if(!world.isSolid(x,y))
				pointsList.add(new Point(x,y));
		}
		return pointsList.toArray(new Point[pointsList.size()]);
	}
	
	public int[] getCurrent(){
		//returns an up, down, left or right unit vector pointing in the direction of moving water.
		int[] velocity={0,0};
		for(int i=-1;i<2;i++){
			for(int j=-1;j<2;j++){
				int[] dv=world.getWaterVelocityAtPoint(getX()+i,getY()+j);
				velocity[0]+=dv[0];
				velocity[1]+=dv[1];
			}
		}
		
		//make the velocity horizontal or vertical, not diagonal.
		if(Math.abs(velocity[0])>Math.abs(velocity[1]))
			velocity[1]=0;
		else
			velocity[0]=0;
		return velocity;
	}
	
	public boolean isInCurrent(){
		int[] current = getCurrent();
		return current[0]!=0 || current[1]!=0;
	}
	
	public void followCurrent(){
		status = Status.RIDING;
		int[] current = getCurrent();
		clearBehaviours();
		updatePosition(getX()+current[0],getY()+current[1]);
	}
	
	public void turn(){
		status = Status.TURNING;
		setAndTickBehaviour(new WaitBehaviour(5));
	}
	
	public void fall(){
		status = Status.FALLING;
		clearBehaviours();
		updatePosition(getX(),getY()-1);
	}
	
	public void tumble(){
		status = Status.TUMBLING;
		Point[] tumbles = getTumblePoints();
		if(tumbles.length==0){
			Logging.error("fish tumble called with no tumble points");
			return;
		}
		Point tumble = tumbles[Utilities.randomInteger(0, tumbles.length-1)];	
		clearBehaviours();
		updatePosition(tumble.getX(),tumble.getY());
	}
	
	public void flop(){
		status = Status.FLOPPING;
		clearBehaviours();
		int dx=Utilities.randomInteger(0, 1)*2-1;
		int flopPower=Utilities.randomInteger(1, 4);
		
		int[][] moves=new int[0][0];
		if(flopPower==1 || energy < STAT_MAX/3){
			moves=new int[][]{{dx,1},{dx,-1}};
		}else if(flopPower==2 || energy < STAT_MAX*2/3){
			moves=new int[][]{{0,1},{dx,1},{dx,-1},{0,-1}};
		}else if(flopPower==3){
			moves=new int[][]{{0,1},{dx,1},{dx,1},{dx,-1},{dx,-1},{0,-1}};
		}else if(flopPower==4){
			moves=new int[][]{{0,1},{0,1},{dx,1},{dx,1},{dx,-1},{dx,-1},{0,-1},{0,-1}};
		}else{
			changeEnergy(5);
		}
		
		for(int i=0;i<moves.length;i++){
			int[] move = moves[i];
			behaviours.add(new MoveBehaviour(move[0],move[1]));
		}
		
		tickNextBehaviour();
	}
	
	private boolean hasBehavioursInQueue(){
		return behaviours.size()>0;
	}
	
	public void frolic(){
		status = Status.FROLIC;
		if(!hasBehavioursInQueue() && Utilities.random0To1()<0.2f){
			turn();
			return;
		}
		//random direction -1 or 1
		int delta = Utilities.randomInteger(0, 1)*2-1;
		int[] waits = new int[]{1,1,1,1,2,2,2,3,3,4,4,5,6,8};
		if(Utilities.random0To1()<0.2f){
			//move a little up or down
			for(int i=Utilities.randomInteger(0, 5);i<Utilities.randomInteger(waits.length-4, waits.length);i++){
				behaviours.add(new MoveBehaviour(0,delta));
				behaviours.add(new WaitBehaviour(waits[i]));
			}
		}else{
			//dart left or right
			for(int i=Utilities.randomInteger(0, 5);i<Utilities.randomInteger(waits.length-4, waits.length);i++){
				behaviours.add(new MoveBehaviour(delta,0));
				behaviours.add(new WaitBehaviour(waits[i]));
			}
		}
		
		tickNextBehaviour();
	}
	
	public void rest(){
		status=Status.RESTING;
		if(isUnderwater()){
			changeOxygen(3);
		}
		if(oxygen>STAT_MAX/2){
			changeEnergy(3);
			changeHitPoints(1);
		}else{
			changeEnergy(1);
		}
	}
	
	public boolean shouldRest(){
		return hitPoints<STAT_MAX || oxygen < STAT_MAX*8/10 || energy<STAT_MAX/2;
	}

	@Override
	public void tick() {
		if(status==Status.DEAD){
			deathTicks++;
			if(deathTicks>DEATH_TICK_DELETION){
				world.removeEntityFromWorld(this);
				world.notifyObserversEntityMove(this);
			}
		}else{
			finFlapCount++;
			
			if(isUnderwater()){
				changeOxygen(2);
				if(isInCurrent()){
					followCurrent();
				}else if(isAlive() && !hasBehavioursInQueue()){
					if(shouldRest()){
						rest();
					}else{
						frolic();
					}
				}
			}else{
				//not underwater
				changeOxygen(-1);
				if(hasBehavioursInQueue()){
					tickNextBehaviour();
				}else{
					if(canFall()){
						changeHitPoints(-1);
						fall();
					}else if(canTumble()){
						changeHitPoints(-5);
						tumble();
					}else if(isAlive()){
						flop();
					}
				}
			}
		}
		
		if(hasMovedThisTick()){
			int[] old = getOldPosition();
			for(int x=1;x<3;x++){
				world.notifyObserversCellChange(old[0]+x, old[1]);
				world.notifyObserversCellChange(old[0]-x, old[1]);
			}
			world.notifyObserversEntityMove(this);
		}
	}

}











