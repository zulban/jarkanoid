package agents;
import java.util.Arrays;

import generic.Logging;
import generic.Utilities;
import simulation.World;

public class Ball extends Agent {
	private float[] velocity = { 0, 0 };
	private float[] velocityAddition = { 0, 0 };
	private int waterTicks = 0;

	public Ball(World world, int x, int y) {
		super(world, x, y);

		if (x < 0 || y < 0)
			setRandomPosition();
		setRandomVelocity();
	}

	@Override
	public void tick() {
		if (world.isPositionWater(getPosition()) && waterTicks < 3) {
			waterTicks++;
			return;
		}
		waterTicks = 0;

		for (int i = 0; i < 2; i++)
			velocityAddition[i] += Math.abs(velocity[i]);
		int changedComponentIndex = velocityAddition[0] > velocityAddition[1] ? 0 : 1;
		velocityAddition[changedComponentIndex] = 0;
		int componentDelta = velocity[changedComponentIndex] > 0 ? 1 : -1;
		int newX = getX() + (changedComponentIndex == 0 ? componentDelta : 0);
		int newY = getY() + (changedComponentIndex == 1 ? componentDelta : 0);

		if (world.isSolid(newX, newY)) {
			world.hitBlock(newX, newY, 1);
			velocity[changedComponentIndex] *= -1;
		} else {
			updatePosition(newX, newY);
		}
	}

	public void setRandomVelocity() {
		velocity = new float[] { 0, 0 };
		float minimum = 0.1f;
		while (velocity[0] < minimum && velocity[1] < minimum) {
			float radians = (float) (Utilities.random0To1() * Math.PI * 2);
			float magnitude = 5;
			velocity[0] = (float) (Math.cos(radians) * magnitude);
			velocity[1] = (float) (Math.sin(radians) * magnitude);
		}
		velocityAddition = new float[] { 0, 0 };
	}
}
