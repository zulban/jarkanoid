package agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import generic.Logging;
import generic.Utilities;
import simulation.World;

public class Water extends Agent {

	private final static int RECALCULATE_MAX = 5;

	private static int waterCounter = 0;

	private int depth = 0;
	private int lastDepthTick = 0;
	private int recalcCounterThisTick = 0;
	private int[] velocityThisTick = { 0, 0 };

	public Water(World world, int x, int y) {
		super(world, x, y);
		waterCounter++;
		move(x, y);
	}

	private void move(int[] position) {
		move(position[0], position[1]);
	}

	private void move(int newX, int newY) {
		// generic move water
		Logging.debug("water.move " + this);
		velocityThisTick = new int[] { newX - getX(), newY - getY() };

		updatePosition(newX, newY);
		if (canRise())
			setDepth(0);
	}
	
	public int[] getVelocityThisTick(){
		return Arrays.copyOf(velocityThisTick, 2);
	}

	public boolean canFall() {
		// returns true if water can fall straight down.
		int x = getX();
		int y = getY() - 1;
		return world.isWaterPermeable(x, y);
	}

	public boolean canTumble() {
		// returns true if water can tumble down-left or down-right.
		return getTumbleShufflePositions(true).size() > 0;
	}

	public boolean canRise() {
		// returns true if water can go straight up
		int x = getX();
		int y = getY() + 1;
		return world.isWaterPermeable(x, y);
	}

	public boolean hasWaterTension() {
		// returns true if water is beside another water
		return world.isWaterAtPosition(getX() - 1, getY()) || world.isWaterAtPosition(getX() + 1, getY());
	}

	private List<int[]> getTumbleShufflePositions(boolean isTumble) {
		// returns coordinates where water can tumble or shuffle.
		// tumble means down-left or down-right."
		// shuffle (tumble=false) means left or right.
		
		List<int[]> tumbles = new ArrayList<int[]>();
		for (int i = -1; i < 2; i += 2) {
			int newX = getX() + i;
			int newY = getY() - (isTumble ? 1 : 0);
			if (world.isWaterPermeable(newX, newY)) {
				tumbles.add(new int[] { newX, newY });
			}
		}

		return tumbles;
	}

	public List<int[]> getBlobMovePositions() {
		// returns coordinates where water can blob move.
		// looks at water just below this, and returns the left and right side
		// coordinates of that water row, if air.

		// ..........w
		// L~~~~~~~~~~~~~~~~R
		// if there are two sides, first item in list is closest to 'w''.

		int centerX = getX();
		int centerY = getY() - 1;
		List<int[]> blobMoves = new ArrayList<int[]>();

		if (!world.isWaterAtPosition(centerX, centerY))
			return blobMoves;

		for (int direction = -1; direction < 2; direction += 2) {
			int[] point = new int[] { centerX, centerY };
			Water edgeWater = null;
			while (world.isWaterAtPosition(point)) {
				edgeWater = world.getWaterAtPosition(point);
				point[0] += direction;
			}
			if (edgeWater != null && !edgeWater.hasMovedThisTick() && world.isWaterPermeable(point))
				blobMoves.add(point);
		}

		// Make it so the first blobMove is the closest to centerX
		if (blobMoves.size() == 2
				&& Math.abs(centerX - blobMoves.get(0)[0]) > Math.abs(centerX - blobMoves.get(1)[0])) {
			blobMoves.add(blobMoves.get(0));
			blobMoves.remove(0);
		}

		return blobMoves;
	}

	private void setDepth(int depth) {
		// this is how water above this water messages this water what its depth
		// should be (at least).

		if (lastDepthTick == world.getTickCounter()) {
			recalcCounterThisTick++;
			if (recalcCounterThisTick > RECALCULATE_MAX || this.depth > depth)
				return;
		} else {
			recalcCounterThisTick = 0;
			lastDepthTick = world.getTickCounter();
		}

		this.depth = depth;
		Water water;

		// water below
		water = world.getWaterAtPosition(getX(), getY() - 1);
		if (water != null)
			water.setDepth(depth + 1);

		// water beside
		for (int i = -1; i < 2; i += 2) {
			water = world.getWaterAtPosition(getX() + i, getY());
			if (water != null)
				water.setDepth(depth);
		}

		// water above
		water = world.getWaterAtPosition(getX(), getY() + 1);
		if (this.depth > 0 && water != null)
			water.setDepth(depth - 1);
	}

	private void fall() {
		// fall straight down.
		Logging.debug("fall " + this);

		setDepth(0);
		move(getX(), getY() - 1);
	}

	private void tumble() {
		// tumble either down-left or down-right, whichever is available, or
		// randomly if both available.
		Logging.debug("tumble " + this);

		setDepth(0);
		List<int[]> tumbles = getTumbleShufflePositions(true);
		if (tumbles.size() == 0) {
			Logging.error("tumble failed, no tumble positions!");
			return;
		}
		int index = Utilities.randomInteger(0, tumbles.size() - 1);
		move(tumbles.get(index)[0], tumbles.get(index)[1]);
	}

	private void shuffle() {
		// shuffle either left or right, whichever is available, or randomly if
		// both available.
		Logging.debug("shuffle " + this);

		setDepth(0);
		List<int[]> tumbles = getTumbleShufflePositions(false);
		if (tumbles.size() == 0) {
			Logging.error("tumble failed, no tumble positions!");
			return;
		}
		int index = Utilities.randomInteger(0, tumbles.size() - 1);
		move(tumbles.get(index)[0], tumbles.get(index)[1]);
	}

	private void rise() {
		// rise straight up
		Logging.debug("rise " + this);

		move(getX(), getY() + 1);
	}

	private void blobMove() {
		// moves to nearest left or right slot on the row of water this water
		// sits atop.
		// only if edge water has not moved yet.
		Logging.debug("blobMove " + this);

		List<int[]> positions = getBlobMovePositions();
		if (positions.size() == 0) {
			Logging.error("blobMove failed, no blob moves!");
			return;
		}
		int[] position = positions.get(0);
		move(position);
	}

	public boolean canShuffle() {
		// returns true if water can shuffle left or right.
		return getTumbleShufflePositions(false).size() > 0;
	}

	public boolean canBlobMove() {
		// returns true if the row of water it sits atop has air on its left or
		// right. Also, those edge waters have not moved this tick.

		return getBlobMovePositions().size() > 0;
	}

	public int getDepth() {
		return depth;
	}

	@Override
	public void tick() {
		velocityThisTick = new int[] { 0, 0 };

		if (canFall())
			fall();
		else if (canTumble())
			tumble();

		if (!hasMovedThisTick()) {
			if (depth == 0 && canBlobMove())
				blobMove();
			else if (depth > 0 && canShuffle())
				shuffle();
			else if (depth > 1 && canRise())
				rise();
		}
	}

}
