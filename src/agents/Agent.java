package agents;
import java.util.Arrays;

import generic.Utilities;
import simulation.Entity;
import simulation.World;

public abstract class Agent extends Entity {
	//Agents get a tick signal from the simulation every turn.

	public abstract void tick();

	public Agent(World world, int x, int y) {
		super(world, x, y);
	}

}
