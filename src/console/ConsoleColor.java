package console;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import generic.Logging;

public enum ConsoleColor {
	ALPHA(-1), RED(41), GREEN(42), ORANGE(43), BLUE(44), PURPLE(45), AQUA(46), SILVER(47), GREY(100), PINK(101), LIME(
			102), YELLOW(103), SKY(104), MAGENTA(105), TURQUOISE(106);

	private int value;

	// Internal hackery to retrieve values from enums
	private static HashMap<Integer, ConsoleColor> map = new HashMap<>();

	// All colors in random order.
	private static ArrayList<ConsoleColor> randomWallColors;

	private ConsoleColor(int value) {
		this.value = value;
	}

	static {
		for (ConsoleColor color : ConsoleColor.values()) {
			map.put(color.value, color);
		}
	}

	public static void printRed(String text) {
		printColor(text, ConsoleColor.RED, true);
	}

	public static void printGreen(String text) {
		printColor(text, ConsoleColor.GREEN, true);
	}

	public static void printYellow(String text) {
		printColor(text, ConsoleColor.YELLOW, true);
	}

	public static void printOrange(String text) {
		printColor(text, ConsoleColor.ORANGE, true);
	}

	public static void printBlue(String text) {
		printColor(text, ConsoleColor.BLUE, true);
	}

	public static void printColor(String text, ConsoleColor color) {
		printColor(text, color, true);
	}

	public static void printColor(String text, ConsoleColor color, boolean newline) {
		if(newline)
			text=text.concat((newline ? "\n" : ""));
		if (canPrintBashColors()) {
			String toPrint = "\033[" + (color.getValue() - 10) + "m" + text + "\033[0m";
			System.out.print(toPrint);
		} else {
			System.out.print(text);
		}
	}

	public static boolean canPrintBashColors() {
		// Returns true if the console is able to print with these bash colors.
		// For now, assume yes.
		return true;
	}

	public static ConsoleColor valueOf(int color) {
		return map.get(color);
	}

	public int getValue() {
		return value;
	}
	
	public static void randomizeWallColors(){
		randomWallColors = new ArrayList<ConsoleColor>();
		map.values().forEach((color) -> {
			if (color != ConsoleColor.ALPHA)
				randomWallColors.add(color);
		});
		Collections.shuffle(randomWallColors);
		for (int i = 0; i < randomWallColors.size(); i++)
			Logging.debug("setting randomWallColors(" + i + ") = " + randomWallColors.get(i));
	}

	public static ConsoleColor getWallColor(int index) {
		if (randomWallColors == null) {
			randomizeWallColors();
		}
		if (index < randomWallColors.size())
			return randomWallColors.get(index);
		return ConsoleColor.GREEN;
	}
}