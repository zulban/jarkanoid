package console;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import generic.Logging;
import simulation.World;
import simulation.WorldFactory;

public class Jarkanoid {
	private final static String HELP = "Jarkanoid! A revamped Zarkanoid written in Java.\n\n    Usage:"
			+ "\njava Jarkanoid terminal <path-to-png> [options]" 
			+ "\njava Jarkanoid color-test" 
			+ "\n\n    Options:"
			+ "\n--help -h" 
			+ "\n--ms=<interval>      Wait this many milliseconds between ticks. [default: 50]"
			+ "\n--age=<ticks>        After this many ticks, restart the world. Zero means never restart. [default: 0]"
			+ "\n--cycles=<count>     After this many world restarts, end the program. [default: 1000]" 
			+ "\n--verbose"
			+ "\n--log";

	private static String[] args;

	public static void main(String[] args) {
		Jarkanoid.args = args;

		if (hasOption("-h") || hasOption("--help")) {
			showHelp();
			return;
		}

		if (hasUnknownOptions()) {
			System.out.println("Unknown command line argument: '" + getUnknownOptions() + "'");
			showHelp();
			return;
		}

		Logging.setOutputs(hasOption("--verbose"), hasOption("--log"));

		if (hasCommand("terminal") && args.length >= 2) {
			String path = args[1];

			int age = getIntArgument("--age");
			int cycles = getIntArgument("--cycles");
			long ms = (long) getFloatArgument("--ms");

			while (cycles > 0) {
				World world = WorldFactory.createWorldFromPNG(path);
				world.registerObserver(new ConsoleView(world));
				world.loop(ms, age);
				cycles--;
			}
			return;
		} else if (hasCommand("color-test")) {
			for (int i = 0; i < 100; i++)
				System.out.print("\n");
			System.out.print("\033[32m\033[5;10Hcolortest\033[0m");
		} else {
			showHelp();
		}
	}

	private static String getUnknownOptions() {
		// Returns a string with the first invalid/unknown command line
		// argument.
		// Returns an empty string if all appear valid.
		String[] lines = HELP.split("\n");
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (!arg.startsWith("--"))
				continue;
			if (arg.contains("="))
				arg = arg.split("=")[0];
			boolean found = false;
			for (int j = 0; j < lines.length; j++) {
				String line = lines[j];
				if (line.startsWith(arg)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return arg;
			}
		}
		return "";
	}

	private static boolean hasUnknownOptions() {
		return getUnknownOptions().length() > 0;
	}

	private static float getFloatDefault(String argument) {
		// given an argument like '--wait' returns the default value, or zero if
		// none is found.
		Pattern pattern = Pattern.compile(argument + ".*default:[ ]*([0-9]+(\\.[0-9]+)?)");
		Matcher matcher = pattern.matcher(HELP);
		if (matcher.find()) {
			return Float.parseFloat(matcher.group(1).toString());
		}
		return 0;
	}

	private static int getIntArgument(String argument) {
		return (int) getFloatArgument(argument);
	}

	private static float getFloatArgument(String argument) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.startsWith(argument) && arg.contains("=")) {
				String[] splits = arg.split("=");
				String suffix = splits[splits.length - 1];
				return Float.parseFloat(suffix);
			}
		}
		return getFloatDefault(argument);
	}

	private static boolean hasCommand(String command) {
		// Returns true if the command line args contains this command, like
		// java Jarkanoid unittest
		return args.length >= 1 && args[0].equals(command);
	}

	private static boolean hasOption(String flag) {
		// Returns true if the command line args contains this option, like
		// --verbose
		return Arrays.asList(args).contains(flag);
	}

	public static void showHelp() {
		System.out.println(HELP);
	}
}
