package console;

import java.util.ArrayList;

import agents.Agent;
import agents.Ball;
import agents.Fish;
import agents.Water;
import blocks.Air;
import blocks.Block;
import blocks.Bomb;
import blocks.Frozen;
import blocks.Rock;
import blocks.Unbreakable;
import generic.Logging;
import generic.Utilities;
import simulation.Entity;
import simulation.Observer;
import simulation.World;

public class ConsoleView implements Observer {
	private World world;

	public ConsoleView(World world) {
		this.world = world;
		ConsoleColor.randomizeWallColors();
		redrawAll();
	}

	public void redrawAll() {
		for (int i = 0; i < 100; i++)
			System.out.print("\n\n\n\n\n");
		for (int x = 0; x < world.getWidth(); x++)
			for (int y = 0; y < world.getHeight(); y++)
				draw(x, y, " ", ConsoleColor.ALPHA);
		redraw(0, 0, world.getWidth() - 1, world.getHeight() - 1);
	}

	public void redraw(int x1, int y1, int x2, int y2) {
		// Redraws the square of cells where (x1,y1) is bottom left and (x2,y2)
		// is top right, inclusive.

		if (x1 > x2 || y1 > y2) {
			Logging.warning(
					"redraw got weird values for (x1,x2,y1,y2) = (" + x1 + "," + x2 + "," + y1 + "," + y2 + ")");
			return;
		}

		for (int i = x1; i <= x2; i++) {
			for (int j = y1; j <= y2; j++) {
				if (!world.isPositionInWorld(i, j)) {
					Logging.warning("redraw loop got coordinates that are out of this world! (" + i + "," + j + ")");
					continue;
				}

				Entity entity = getEntityToRenderAtPosition(i, j);
				String c = getEntityString(entity);
				ConsoleColor color = getBackgroundColorFromEntity(entity);
				draw(i, j, c, color);
			}
		}
	}

	private Entity getEntityToRenderAtPosition(int x, int y) {
		// Only one entity can be rendered per position. This returns that one
		// that should be rendered.
		Entity entity = getHighestZOrderEntity(x, y);
		if (entity == null)
			entity = (Entity) world.getBlock(x, y);
		return entity;
	}

	private Entity getHighestZOrderEntity(int x, int y) {
		// Returns the entity at this position with the highest render priority.
		Entity[] entities = world.getEntitiesAtPosition(new int[] { x, y });

		Logging.debug("getHighestZOrderEntity (3,3) entities.length=" + entities.length);

		if (entities.length == 0)
			return null;
		else if (entities.length == 1)
			return entities[0];
		else {
			int highestZ = 0;
			Entity highestEntity = entities[0];
			for (int i = 0; i < entities.length; i++) {
				int z = getZOrderFromEntity(entities[i]);
				// Newest entity added wins ties, >=
				if (z >= highestZ) {
					highestZ = z;
					highestEntity = entities[i];
				}
			}
			return highestEntity;
		}
	}

	private int getZOrderFromEntity(Entity entity) {
		// If two entities occupy the same cell, the one with the greater
		// z-order is rendered.
		if (entity instanceof Water)
			return 1;
		else if (entity instanceof Agent)
			return 2;
		return 0;
	}

	private ConsoleColor getBackgroundColorFromEntity(Entity entity) {
		if (entity == null || (entity instanceof Air)) {
			return ConsoleColor.ALPHA;
		} else if (entity instanceof Ball || entity instanceof Fish) {
			return entity.isUnderwater()?ConsoleColor.BLUE:ConsoleColor.ALPHA;
		} else if (entity instanceof Water) {
			return ConsoleColor.BLUE;
		} else if (entity instanceof Rock) {
			int hp = ((Rock) entity).getHitPoints();
			int index = Utilities.clamp(hp, 0, 9);
			return ConsoleColor.getWallColor(index);
		} else if (entity instanceof Bomb) {
			return ConsoleColor.RED;
		} else if (entity instanceof Frozen) {
			return ConsoleColor.AQUA;
		} else if (entity instanceof Unbreakable) {
			return ConsoleColor.YELLOW;
		} else {
			return ConsoleColor.GREY;
		}
	}
	
	public static void printWorld(World world){
		System.out.println(getWorldString(world));
	}

	public static String getWorldString(World world) {
		int width=world.getWidth();
		int height=world.getHeight();
		
		String header = "<World" + width + "x" + height + ">\n";
		String[][] mapString = new String[height][width];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Entity[] entities = world.getEntitiesAtPosition(new int[] { i, j });
				String c;
				if (entities.length == 0)
					c="-";
				else if (entities.length == 1)
					c = getEntityString(entities[0]);
				else
					c = "*";
				mapString[j][i] = c;
			}
		}
		String[] lines = new String[height];
		for(int j=0;j<height;j++)
			lines[j] = String.join("", mapString[height-j-1]);
		String body = String.join("\n", lines);
		return header+body;
	}
	
	private static String getFishString(Fish fish){
		if (fish.status==Fish.Status.DEAD){
			if(fish.getDeathTicks()<50)
				return fish.direction==Fish.Direction.LEFT?"(xx)_":"_(xx)";
			else if(fish.getDeathTicks()<100)
				return fish.direction==Fish.Direction.LEFT?".xx._":"_.xx.";
			else if(fish.getDeathTicks()<200)
				return fish.direction==Fish.Direction.LEFT?".,,..":"..,,.";
			else if(fish.getDeathTicks()<500)
				return fish.direction==Fish.Direction.LEFT?" ,,  ":"  ,, ";
			else
				return "";
		}else{
			if(fish.direction==Fish.Direction.TURNING)
				return "('o')";
			
			String eye="o";
			if(fish.isHappy())
				eye="'";
			else if(fish.isPanicked())
				eye="0";
			
			String left = fish.direction==Fish.Direction.LEFT?"("+eye:"}(";
			String right = fish.direction==Fish.Direction.LEFT?"){":eye+")";
			String fin=fish.isFlappingFin()?(fish.direction==Fish.Direction.LEFT?"<":">"):"v";
			return left+fin+right;
		}
	}

	private static String getEntityString(Entity entity) {
		if (entity == null || (entity instanceof Air))
			return " ";
		else if (entity instanceof Rock)
			return ".";
		else if (entity instanceof Bomb)
			return "b";
		else if (entity instanceof Frozen)
			return "o";
		else if (entity instanceof Unbreakable)
			return "X";
		else if (entity instanceof Ball)
			return "o";
		else if (entity instanceof Fish)
			return getFishString((Fish)entity);
		else if (entity instanceof Water)
			return "~";
		return "?";
	}

	@Override
	public void notifyCellChange(int[] position) {
		int x = position[0];
		int y = position[1];
		redraw(x, y, x, y);
	}

	public void draw(int x, int y, String c, ConsoleColor fillColor) {
		draw(x, y, c, fillColor, ConsoleColor.ALPHA, false);
	}

	public void draw(int x, int y, String c, ConsoleColor fillColor, ConsoleColor charColor, boolean bold) {
		Logging.debug("draw (" + x + "," + y + ") c='" + c + "' fillColor = '" + fillColor.getValue() + "'");

		// adjust the coordinates for bash terminal coordinates.
		// In bash, x and y start at 1, not 0.
		x++;
		// Invert y coordinate. The world's origin is bottom left. Bash is top
		// left.
		y = world.getHeight() - y;

		/*
		 * 1 bold 31-37 foreground 41-47 background 90-97 foreground 100-107
		 * background
		 */

		String prefix = "";
		if (fillColor != null && fillColor != ConsoleColor.ALPHA)
			prefix = prefix.concat("\033[" + fillColor.getValue() + "m");
		if (charColor != null && charColor != ConsoleColor.ALPHA)
			prefix = prefix.concat("\033[" + (charColor.getValue() - 10) + "m");
		if (bold)
			prefix = prefix.concat("\033[1m");

		String content = "\033[" + y + ";" + x + "H" + c;
		String suffix = "\033[0m";
		System.out.print(prefix + content + suffix);
	}
	
	@Override
	public void notifyWorldTick(){
		
		world.getAgents().forEach((agent)->{
			if(agent instanceof Fish){
				notifyEntityMove((Fish)agent);
			}				
		});
		
	}

	@Override
	public void notifyWorldChange() {
		redrawAll();
	}
	
	private int[] getEntityRenderOffset(Entity entity){
		if(entity instanceof Fish)
			return new int[]{-2,0};
		return new int[]{0,0};
	}

	@Override
	public void notifyEntityMove(Entity entity) {
		int x = entity.getX();
		int y = entity.getY();
		if (world.isPositionInWorld(x, y)) {
			String c = getEntityString(entity);
			ConsoleColor backgroundColor = getBackgroundColorFromEntity(entity);
			int[] offset=getEntityRenderOffset(entity);
			draw(entity.getX()+offset[0], entity.getY()+offset[1], c, backgroundColor);
		}

	}
}
