package generic;
import java.util.concurrent.ThreadLocalRandom;

public class Utilities {
	public static int randomInteger(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static float random0To1() {
		// Returns 0 to 1
		return ThreadLocalRandom.current().nextFloat();
	}

	public static int clamp(int value, int min, int max) {
		if (value < min)
			return min;
		if (value > max)
			return max;
		return value;
	}
	
	public static boolean compareArrays(int[] array1, int[] array2) {
		// Returns true if arrays are value equal.
		if (array1.length != array2.length)
			return false;
		for (int i = 0; i < array1.length; i++) {
			if (array1[i]!=array2[i])
				return false;
		}
		return true;
	}
	
	public static int countOccurencesInString(String string,String subString){
		int lastIndex = 0;
		int count = 0;
		while(lastIndex != -1){
		    lastIndex = string.indexOf(subString,lastIndex);
		    if(lastIndex != -1){
		        count ++;
		        lastIndex += subString.length();
		    }
		}
		return count;
	}
}
