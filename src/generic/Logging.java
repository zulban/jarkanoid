package generic;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.StandardOpenOption;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Logging {
	private static boolean writeToConsole, writeToLog;

	public static final int DEBUG = 0;
	public static final int INFO = 1;
	public static final int WARNING = 2;
	public static final int ERROR = 3;

	private static int loggingLevel = DEBUG;

	public static void warning(String text) {
		if (loggingLevel <= WARNING)
			writeEntry("warning", text);
	}

	public static void info(String text) {
		if (loggingLevel <= INFO)
			writeEntry("info", text);
	}

	public static void error(String text) {
		if (loggingLevel <= ERROR)
			writeEntry("error", text);
	}

	public static void debug(String text) {
		if (loggingLevel <= DEBUG)
			writeEntry("debug", text);
	}

	public static void setLoggingLevel(int level) {
		loggingLevel=level;
	}
	
	public static void setWriteToConsole(boolean isEnabled){
		Logging.writeToConsole = isEnabled;
	}
	
	public static void setWriteToLog(boolean isEnabled){
		Logging.writeToLog = isEnabled;
	}

	public static void setOutputs(boolean writeToConsole, boolean writeToLog) {
		Logging.writeToConsole = writeToConsole;
		Logging.writeToLog = writeToLog;
	}

	private static void writeEntry(String level, String text) {
		String entry = level.toUpperCase() + ": " + text;
		if (writeToConsole)
			System.out.println(entry);
		if (writeToLog)
			appendToLog(entry);
	}

	private static boolean hasDeletedLog = false;

	public static void appendToLog(String line) {
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		try {
			File file = new File("log.txt");
			if (!hasDeletedLog) {
				file.delete();
				hasDeletedLog = true;
			}
			file.createNewFile();

			fileWriter = new FileWriter(file.getAbsoluteFile(), true);
			bufferedWriter = new BufferedWriter(fileWriter);

			bufferedWriter.write("\n" + line);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null)
					bufferedWriter.close();
				if (fileWriter != null)
					fileWriter.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
