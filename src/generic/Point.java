package generic;

public class Point {
	private int x, y;
	private int[] xy;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int[] getXY() {
		return new int[] { x, y };
	}

	public float distance(int x, int y) {
		int dx = Math.abs(this.x - x);
		int dy = Math.abs(this.y - y);
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	public float distance(Point point) {
		return distance(point.x, point.y);
	}
}
