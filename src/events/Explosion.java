package events;
import java.util.ArrayList;
import java.util.HashMap;

import generic.Logging;
import generic.Point;
import simulation.World;

public class Explosion extends Event {
	private int maxRadius,x,y;
	private static HashMap<Integer, Point[]> explosionPoints= new HashMap<Integer,  Point[]>();

	public Explosion(World world, int x, int y, int maxRadius, int tickDuration) {
		super(world, tickDuration);
		this.x=x;
		this.y=y;
		this.maxRadius = maxRadius;
	}

	public Point[] getExplosionPoints() {
		int explosionRadius = Math.round((float) maxRadius * (1 - ((float) ticksLeft / (float) tickDuration)));

		if (!explosionPoints.containsKey(explosionRadius)) {
			ArrayList<Point> pointsList = new ArrayList<Point>();
			Point center = new Point(0, 0);
			for (int i = -explosionRadius; i <= explosionRadius; i++) {
				for (int j = -explosionRadius; j <= explosionRadius; j++) {
					Point point = new Point(i, j);
					int length = Math.round(center.distance(point));
					if (explosionRadius > length)
						pointsList.add(point);
				}
			}
			Point[] points = pointsList.toArray(new Point[pointsList.size()]);
			explosionPoints.put(explosionRadius, points);
		}
		return explosionPoints.get(explosionRadius);
	}

	@Override
	public void tick() {
		super.tick();
		if(!isFinished()){
			Point[] points = getExplosionPoints();
			for (int i = 0; i < points.length; i++) {
				world.hitBlock(points[i].getX()+x, points[i].getY()+y, 1);
			}
		}
	}
}
