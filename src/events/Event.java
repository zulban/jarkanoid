package events;

import generic.Logging;
import simulation.World;

public abstract class Event {
	protected int ticksLeft, tickDuration;
	protected World world;

	public Event(World world, int tickDuration) {
		this.world = world;
		this.tickDuration = tickDuration;
		this.ticksLeft = tickDuration;
		
		Logging.debug("Created event: "+this);
	}

	public void tick() {
		ticksLeft--;
	}

	public boolean isFinished() {
		return ticksLeft <= 0;
	}

}
