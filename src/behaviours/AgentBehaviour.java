package behaviours;

public abstract class AgentBehaviour {
	public int ticksLeft, tickDuration;
	
	public AgentBehaviour(int tickDuration){
		this.tickDuration=tickDuration;
		this.ticksLeft=tickDuration;
	}
	
	public boolean isFinished(){
		return ticksLeft<=0;
	}
}
