package behaviours;

public class MoveBehaviour extends AgentBehaviour {
	public int dx,dy;
	public MoveBehaviour(int dx, int dy) {
		super(1);
		this.dx=dx;
		this.dy=dy;
	}

}
