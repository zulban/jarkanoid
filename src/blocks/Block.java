package blocks;
import simulation.Entity;
import simulation.World;

public abstract class Block extends Entity{
	public Block(World world, int x, int y) {
		super(world, x, y);
	}
	// Blocks occupy one space exclusively and do not move.
	public abstract boolean isDestroyed();
	
	// can this be hit and eventually broken
	public abstract boolean isBreakable();
	
	// can entities pass through this block
	public abstract boolean isSolid(); 
}
