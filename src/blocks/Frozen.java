package blocks;

import agents.Ball;
import simulation.World;

public class Frozen extends BreakableBlock {

	public Frozen(World world, int x, int y) {
		super(world, x, y);
	}

	@Override
	public void hit(int damage) {
		super.hit(damage);
		
		if (isDestroyed()) {
			Ball ball = new Ball(world, getX(), getY());
			world.putEntityInWorld(ball);
		}
	}
}
