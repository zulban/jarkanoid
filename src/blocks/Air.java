package blocks;
import simulation.World;

public class Air extends Block{

	public Air(World world, int x, int y) {
		super(world, x, y);
	}

	@Override
	public boolean isDestroyed() {
		return false;
	}

	@Override
	public boolean isBreakable() {
		return false;
	}

	@Override
	public boolean isSolid() {
		return false;
	}

}
