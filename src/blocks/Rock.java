package blocks;
import simulation.World;

public class Rock extends BreakableBlock{

	public Rock(World world, int x, int y, int hitPoints) {
		super(world, x, y);
		totalHitPoints=hitPoints;
		this.hitPoints=hitPoints;
	}
}
