package blocks;
import simulation.World;

public abstract class BreakableBlock extends Block{

	public BreakableBlock(World world, int x, int y) {
		super(world, x, y);
	}

	protected int totalHitPoints=1;
	protected int hitPoints=1;
	
	public void hit(int damage) {
		hitPoints-=damage;
	}
	
	public boolean isDestroyed() {
		return hitPoints<=0;
	}
	
	public boolean isSolid() {
		return !isDestroyed();
	}
	
	public int getHitPoints(){
		return hitPoints;
	}
	
	public boolean isBreakable(){
		return true;
	}

	public void setHitPoints(int totalHitPoints, int hitPoints) {
		this.totalHitPoints=totalHitPoints;
		this.hitPoints = hitPoints;
	}
}
