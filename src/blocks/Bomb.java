package blocks;
import agents.Ball;
import events.Explosion;
import simulation.World;

public class Bomb extends BreakableBlock {

	public Bomb(World world, int x, int y) {
		super(world, x, y);
		
	}
	
	@Override
	public void hit(int damage) {
		super.hit(damage);
		
		if (isDestroyed()) {
			int maxRadius=7;
			int duration=10;
			world.createEvent(new Explosion(world, getX(), getY(), maxRadius, duration));
		}
	}
}
