package blocks;
import simulation.World;

public class Unbreakable extends Block{

	public Unbreakable(World world, int x, int y) {
		super(world, x, y);
	}

	@Override
	public boolean isDestroyed() {
		return false;
	}

	@Override
	public boolean isBreakable() {
		return false;
	}

	@Override
	public boolean isSolid() {
		return true;
	}

}
