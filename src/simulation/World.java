package simulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.swing.text.html.HTMLDocument.Iterator;

import agents.Agent;
import agents.Ball;
import agents.Water;
import blocks.Block;
import blocks.Bomb;
import blocks.BreakableBlock;
import blocks.Frozen;
import blocks.Rock;
import events.Event;
import events.Explosion;
import generic.Logging;
import generic.Point;
import generic.Utilities;

public class World {
	private final int MAX_DIMENSION = 10000;

	private int tickCounter = 0;
	private int[] size;
	private ArrayList<Agent> agents = new ArrayList<Agent>();;
	private ArrayList<Event> events = new ArrayList<Event>();
	private Water[][] waters;
	private Block[][] blocks;
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	private EntityMap entityMap;

	public World(int width, int height) {
		if (width > MAX_DIMENSION || height > MAX_DIMENSION) {
			width = width > MAX_DIMENSION ? MAX_DIMENSION : width;
			height = height > MAX_DIMENSION ? MAX_DIMENSION : height;
			Logging.error("World dimensions cannot be bigger than " + MAX_DIMENSION + ". Creating (" + width + ","
					+ height + ") dimension world instead.");
		}
		size = new int[] { width, height };
		blocks = new Block[width][height];
		waters = new Water[width][height];
		entityMap = new EntityMap(width, height);
		Entity.resetEntityCounter();
	}

	public ArrayList<Agent> getAgents() {
		return agents;
	}

	public void registerObserver(Observer observer) {
		observers.add(observer);
	}

	public int getWidth() {
		return size[0];
	}

	public int getHeight() {
		return size[1];
	}

	public int[] getSize() {
		return size;
	}

	public int getTickCounter() {
		return tickCounter;
	}

	public boolean isWaterAtPosition(int x, int y) {
		return getWaterAtPosition(x, y) != null;
	}

	public boolean isWaterAtPosition(int[] position) {
		return isWaterAtPosition(position[0], position[1]);
	}

	public boolean isWaterPermeable(int[] position) {
		return isWaterPermeable(position[0], position[1]);
	}

	public boolean isWaterPermeable(int x, int y) {
		// Returns true if water can go into this position.
		// Not solid and no water here.
		return isPositionInWorld(x, y) && !isSolid(x, y) && !isWaterAtPosition(x, y);
	}

	public Entity[] getEntitiesAtPosition(int[] position) {
		return entityMap.getEntitiesAtPosition(position);
	}

	public int countEntitiesInWorld() {
		int count = 0;
		for (int i = 0; i < getWidth(); i++)
			for (int j = 0; j < getHeight(); j++)
				count += entityMap.getEntitiesAtPosition(new int[] { i, j }).length;
		return count;
	}

	public Water getWaterAtPosition(int[] position) {
		return getWaterAtPosition(position[0], position[1]);
	}

	public Water getWaterAtPosition(int x, int y) {
		try {
			Water water = waters[x][y];
			if (water == null)
				return null;
			if (water.getX() != x || water.getY() != y) {
				waters[x][y] = null;
				return null;
			}
			return water;
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}

	public int getWaterDepth(int x, int y) {
		return getWaterDepth(new int[] { x, y });
	}

	public int getWaterDepth(int[] position) {
		Water water = getWaterAtPosition(position);
		return water == null ? -1 : water.getDepth();
	}

	public int[] getWaterVelocityAtPoint(int x, int y) {
		int[] point = new int[] { x, y };
		Water water = getWaterAtPosition(point);
		if (water == null)
			return new int[] { 0, 0 };
		else {
			return water.getVelocityThisTick();
		}
	}

	public int countWaterNearPoint(int x, int y, int scanWidth, int scanHeight) {
		// scans a box of this width and height, returns how many water are in
		// that box.
		int count = 0;
		for (int i = -scanWidth / 2; i <= scanWidth / 2; i++) {
			for (int j = -scanHeight / 2; j <= scanHeight / 2; j++) {
				if (isPositionWater(x + i, y + j))
					count++;
			}
		}
		return count;
	}

	public void putEntityInWorld(Entity entity) {
		if (entity instanceof Block)
			blocks[entity.getX()][entity.getY()] = (Block) entity;
		else if (entity instanceof Agent)
			agents.add((Agent) entity);
		updateEntityPositionInWorld(entity);
	}

	public void removeEntityFromWorld(Entity entity) {
		if (entity instanceof Agent)
			agents.remove((Agent) entity);
		entityMap.pop(entity.getPosition(), entity);
		entity.setPositionToVoid();
	}

	public void updateEntityPositionInWorld(Entity entity) {
		if (!isPositionInWorld(entity.getX(), entity.getY())) {
			Logging.error(
					"refreshEntityPositionInWorld failed, not in world! (" + entity.getX() + "," + entity.getY() + ")");
			return;
		}
		entityMap.updatePosition(entity);

		if (entity instanceof Water) {
			if (isWaterAtPosition(entity.getPosition())) {
				Logging.error("putWaterInWorld with water is already there. " + entity);
			} else {
				waters[entity.getX()][entity.getY()] = (Water) entity;
			}
		}
	}

	public void hitBlock(int[] position, int damage) {
		hitBlock(position[0], position[1], damage);
	}

	public void hitBlock(int x, int y, int damage) {
		if (!isPositionInWorld(x, y))
			return;

		Block block = blocks[x][y];
		if (block == null || !(block instanceof BreakableBlock))
			return;

		BreakableBlock breakableBlock = (BreakableBlock) block;

		breakableBlock.hit(damage);
		if (breakableBlock.isDestroyed()) {
			blocks[x][y]=null;
			entityMap.pop(new int[]{x, y},block);
			putEntityInWorld(new Rock(this,x,y,0));			
		}
		notifyObserversCellChange(x, y);
	}

	public void createEvent(Event event) {
		events.add(event);
	}

	public void tickEvents() {
		ArrayList<Event> deadEvents = new ArrayList<Event>();
		for (int i = 0; i < events.size(); i++) {
			Event event = events.get(i);
			event.tick();
			if (event.isFinished()) {
				deadEvents.add(event);
			}
		}
		events.removeAll(deadEvents);
	}

	public boolean isPositionWater(int x, int y) {
		return isPositionWater(new int[] { x, y });
	}

	public boolean isPositionWater(int[] position) {
		return getWaterAtPosition(position) != null;
	}

	public boolean isPositionInWorld(int x, int y) {
		return isPositionInWorld(new int[] { x, y });
	}

	public boolean isPositionInWorld(int[] position) {
		for (int i = 0; i < 2; i++) {
			if (position[i] < 0 || position[i] >= size[i])
				return false;
		}
		return true;
	}

	public boolean isBreakable(int x, int y) {
		return getBlock(x, y).isBreakable();
	}

	public boolean isSolid(int x, int y) {
		return isSolid(new int[] { x, y });
	}

	public boolean isSolid(int[] position) {
		// Returns true if agents cannot move into this position.
		if (!isPositionInWorld(position))
			return true;
		Block block = getBlock(position);
		return block != null && block.isSolid();
	}

	public Block getBlock(int x, int y) {
		return getBlock(new int[] { x, y });
	}

	public Block getBlock(int[] position) {
		if (!isPositionInWorld(position))
			return null;
		return blocks[position[0]][position[1]];
	}

	public void notifyObserversCellChange(int x, int y) {
		notifyObserversCellChange(new int[] { x, y });
	}

	public void notifyObserversCellChange(int[] position) {
		Logging.debug("notifyObserversCellChange position = " + Arrays.toString(position));
		observers.forEach((observer) -> {
			observer.notifyCellChange(position);
		});
	}

	public void notifyObserversWorldChange() {
		observers.forEach((observer) -> {
			observer.notifyWorldChange();
		});
	}

	public void notifyObserversEntityMove(Entity entity) {
		observers.forEach((observer) -> {
			observer.notifyEntityMove(entity);
		});
	}

	public void loop(long sleepInterval, int worldLifetime) {
		// Ticks for worldLifetime number of ticks.
		// If worldLifetime is zero, loops forever.
		if (getWidth() == 0 || getHeight() == 0) {
			Logging.warning("World width or height is zero. Aborted loop.");
			return;
		}

		if (tickCounter == 0) {
			Logging.info("Starting world loop.");
		} else {
			Logging.info("Continuing world loop.");
		}

		while (worldLifetime == 0 || tickCounter < worldLifetime) {
			tick();
			try {
				Thread.sleep(sleepInterval);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public void tick(int tickCount) {
		// Runs many turns of the world simulation.
		for (int i = 0; i < tickCount; i++)
			tick();
	}

	public void tickAgents() {
		Logging.debug("ticking " + agents.size() + " agents.");
		for (int i = 0; i < agents.size(); i++) {
			Agent agent = agents.get(i);
			agent.tick();
		}
	}

	public void tick() {
		// Runs one turn of the world simulation.
		tickCounter++;
		tickAgents();
		tickEvents();

		// notify observers that a tick occurred
		observers.forEach((observer) -> {
			observer.notifyWorldTick();
		});

		Logging.debug("loop tickCounter = " + tickCounter);
	}

	public int[] getRandomPosition() {
		return new int[] { Utilities.randomInteger(0, getWidth() - 1), Utilities.randomInteger(0, getHeight() - 1) };
	}
}
