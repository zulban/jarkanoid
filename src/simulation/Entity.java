package simulation;
import java.util.Arrays;

import generic.Logging;
import generic.Utilities;

public abstract class Entity {
	//An entity exists somewhere in the world and can be viewed.
	
	private static int entityCounter=0;
	private int entityID=-1;
	
	protected World world;
	private int[] position={-1,-1};
	private int[] oldPosition={-1,-1};
	private int tickCountForOldPosition = 0;
	
	public Entity(World world, int x, int y){
		this.world=world;
		position=new int[]{x,y};

		entityCounter++;
		entityID=entityCounter;
		Logging.debug("Created new entity: "+this.toString());
	}
	
	public void setPositionToVoid(){
		position=new int[]{-1,-1};
	}
	
	public boolean isUnderwater(){
		return world.isWaterAtPosition(position);
	}
	
	public static void resetEntityCounter(){
		entityCounter=0;
	}

	public int[] getOldPosition() {
		return Arrays.copyOf(oldPosition, 2);
	}
	
	public int[] getPosition(){
		return Arrays.copyOf(position, 2);
	}
	
	public int getX(){
		return position[0];
	}
	
	public int getY(){
		return position[1];
	}
	
	public String toString(){
		return "<Entity"+entityID+" at ("+getX()+","+getY()+") of type '"+this.getClass().getName()+"'>";
	}
	
	public boolean hasMovedThisTick(){
		return position[0]!=-1 && tickCountForOldPosition==world.getTickCounter() && !Utilities.compareArrays(position, oldPosition);
	}
	
	public int getTickCountForOldPosition() {
		return tickCountForOldPosition;
	}

	public void updatePosition(int newX, int newY) {
		tickCountForOldPosition=world.getTickCounter();
		oldPosition = Arrays.copyOf(position,2);
		position = new int[] { newX, newY };
		
		world.updateEntityPositionInWorld(this);
		
		world.notifyObserversCellChange(oldPosition);
		world.notifyObserversCellChange(position);
		world.notifyObserversEntityMove(this);
	}

	public boolean hasMoved(int[] oldPosition) {
		return position[0] != oldPosition[0] || position[1] != oldPosition[1];
	}
	public void setRandomPosition() {
		position = world.getRandomPosition();
	}
	public void clampToBoard() {
		for (int i = 0; i < 2; i++)
			position[i] = Utilities.clamp(position[i], 0, world.getSize()[i]);
	}
}
