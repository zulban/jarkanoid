package simulation;
import agents.Agent;

public interface Observer {
	public void notifyCellChange(int[] position);
	public void notifyWorldChange();
	public void notifyEntityMove(Entity entity);
	public void notifyWorldTick();
}
