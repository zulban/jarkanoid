package simulation;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import javax.imageio.ImageIO;

import agents.Agent;
import agents.Ball;
import agents.Fish;
import agents.Water;
import blocks.Air;
import blocks.Block;
import blocks.Bomb;
import blocks.Frozen;
import blocks.Rock;
import blocks.Unbreakable;
import console.ConsoleColor;
import generic.Logging;
import generic.Utilities;

public class WorldFactory {

	public final static int MAX_WALL_HITS = 9;

	public static World createWorldFromPNG(String imagePath) {
		Logging.info("Generating world from: '" + imagePath + "'");

		try {
			BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
			WritableRaster raster = bufferedImage.getRaster();

			World world = new World(raster.getWidth(), raster.getHeight());
			ImageReader.setWorldWithRaster(world, raster);
			return world;

		} catch (IOException e) {
			e.printStackTrace();
		}

		Logging.error("Failed to create world from PNG.");
		return new World(0, 0);
	}

	public static World createWorldFromString(String waterCharMap) {
		// Starting with the top row.
		// String example: "ww--/w---/-ww-"
		// w=water
		// 1-9=rock
		// f=frozen
		// o=ball
		// b=bomb

		char rowDelimit = '/';
		int width;
		if (waterCharMap.contains(Character.toString(rowDelimit)))
			width = waterCharMap.indexOf(rowDelimit);
		else
			width=waterCharMap.length();
		String[] lines = waterCharMap.split("/");
		int height = lines.length;

		for (int y = 0; y < lines.length; y++)
			if ((lines[y]).length() != width)
				ConsoleColor.printRed("buildWaterWorld failed. Got a bad waterCharMap: '" + waterCharMap + "'");

		World world = new World(width, height);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				char c = (lines[y]).charAt(x);
				int inverseY = height - y - 1;
				if (c == 'w') {
					world.putEntityInWorld(new Water(world, x, inverseY));
				} else if (c == 'o') {
					world.putEntityInWorld(new Ball(world, x, inverseY));
				} else if (c == 'f') {
					world.putEntityInWorld(new Frozen(world, x, inverseY));
				} else if (c == 'b') {
					world.putEntityInWorld(new Bomb(world, x, inverseY));
				} else if (c == 'X') {
					world.putEntityInWorld(new Unbreakable(world, x, inverseY));
				} else if (Character.isDigit(c)) {
					int hitPoints = Integer.parseInt(Character.toString(c));
					world.putEntityInWorld(new Rock(world, x, inverseY, hitPoints));
				}
			}
		}
		return world;
	}

}
