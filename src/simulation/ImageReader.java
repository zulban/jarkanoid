package simulation;

import java.awt.image.WritableRaster;
import java.util.Arrays;

import agents.*;
import blocks.*;
import generic.Logging;
import generic.Utilities;

public class ImageReader {

	public static void setWorldWithRaster(World world, WritableRaster raster) {
		int valuesPerPixel = 4;// RGBA
		int[] pixelArray = new int[raster.getWidth() * raster.getHeight() * valuesPerPixel];
		raster.getPixels(0, 0, raster.getWidth(), raster.getHeight(), pixelArray);

		int x = 0;
		int y = 0;
		for (int i = 0; i < pixelArray.length; i += valuesPerPixel) {
			int[] rgb = Arrays.copyOfRange(pixelArray, i, i + 3);
			int invertedY = raster.getHeight() - y - 1; //PNG origin is top left, World origin is bottom left.
			setWorldWithPixel(world, x, invertedY, rgb);

			x++;
			if (x >= raster.getWidth()) {
				x = 0;
				y++;
			}
		}
	}

	private static void setWorldWithPixel(World world, int x, int y, int[] rgb) {
		Logging.debug("setWorldWithPixel (" + x + "," + y + ") rgb=" + Arrays.toString(rgb));

		Entity entity = ImageReader.getEntityFromRGB(world, x, y, rgb);
		world.putEntityInWorld(entity);
		if(entity == null){
			Logging.error("setWorldWithPixel failed, got null entity. rgb = " + Arrays.toString(rgb));
		}
	}
	
	public static Entity getEntityFromRGB(World world, int x, int y, int[] rgb) {
		Entity entity;
		if (Utilities.compareArrays(rgb, new int[] { 255, 0, 255 })) {
			entity = new Ball(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 255, 255, 255 })) {
			entity = new Air(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 0, 0, 255 })) {
			entity = new Water(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 0, 255, 0 })) {
			entity = new Fish(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 0, 255, 255 })) {
			entity = new Frozen(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 255, 0, 0 })) {
			entity = new Bomb(world, x, y);
		} else if (Utilities.compareArrays(rgb, new int[] { 255, 255, 0 })) {
			entity = new Unbreakable(world, x, y);
		} else {
			int hitPoints = getBreakableHitPointsFromRGB(rgb);
			entity = new Rock(world, x, y, hitPoints);
		}
		return entity;
	}

	public static int getBreakableHitPointsFromRGB(int[] rgb) {
		// Assuming this is a rock, returns hit points from [1,MAX_WALL_HITS]
		// Black is max, light grey is minimum.
		int sum=rgb[0]+rgb[1]+rgb[2];
		float highest=255*3;
		float score=highest-sum;
		int hp=Math.round((score / highest) * WorldFactory.MAX_WALL_HITS);
		return hp>0?hp:1;
	}
}
