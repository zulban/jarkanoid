package simulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import generic.Logging;

public class EntityMap {
	public final static int BUCKET_SIZE = 5;

	Entity[][][] entityArray;
	int[][] entityCount;
	ArrayList<Entity> overflow = new ArrayList<Entity>();

	public EntityMap(int width, int height) {
		entityArray = new Entity[width][height][BUCKET_SIZE];
		entityCount = new int[width][height];
	}

	public void updatePosition(Entity entity) {
		// Removes the entity from its old position (if it's there) and moves it
		// to the new one.
		pop(entity.getOldPosition(), entity);
		add(entity);
	}

	private void add(Entity entity) {
		int x = entity.getX();
		int y = entity.getY();
		int index = getEmptyBucketIndex(x, y);
		if (index < BUCKET_SIZE) {
			entityArray[x][y][index] = entity;
		} else {
			overflow.add(entity);
		}
		entityCount[x][y]++;
	}

	private int getEmptyBucketIndex(int x, int y) {
		// Finds the index of this bucket's null value, with the lowest index.
		// Returns BUCKET_SIZE if bucket is full.
		for (int i = 0; i < BUCKET_SIZE; i++)
			if (entityArray[x][y][i] == null)
				return i;
		return BUCKET_SIZE;
	}

	private int countEntitiesAtPosition(int[] position) {
		// Returns the number of entities in the bucket at this position.
		try {
			return entityCount[position[0]][position[1]];
		} catch (ArrayIndexOutOfBoundsException e) {
			return 0;
		}
	}

	public boolean pop(int[] position, Entity entity) {
		// Returns true if it successfully popped an entity.
		int count = countEntitiesAtPosition(position);
		if (count == 0) {

			// No entities to remove in this bucket.
			return false;
		} else {

			// Remove from bucket, if it's there.
			int x = position[0];
			int y = position[1];
			for (int i = 0; i < BUCKET_SIZE; i++) {
				if (entity == entityArray[x][y][i]) {
					entityArray[x][y][i] = null;
					entityCount[x][y]--;
					return true;
				}
			}

			// Remove from overflow, if bucket is overflowed
			if (count == BUCKET_SIZE && overflow.contains(entity)) {
				overflow.remove(entity);
				entityCount[x][y]--;
				return true;
			}

			// Did not remove from bucket or overflow.
			return false;
		}
	}

	private Entity[] getOverflowEntitiesAtPosition(int[] position) {
		// Ignoring the buckets, returns all entities, if any, at this position
		// in the overflow pile.
		ArrayList<Entity> matching = new ArrayList<Entity>();
		overflow.forEach((candidate) -> {
			if (candidate.getX() == position[0] && candidate.getY() == position[1])
				matching.add(candidate);
		});
		return matching.toArray(new Entity[matching.size()]);
	}

	public Entity[] getEntitiesAtPosition(int[] position) {
		int count = countEntitiesAtPosition(position);
			
		if (count == 0) {
			return new Entity[0];
		} else if (count < BUCKET_SIZE) {
			// Return all entities in the bucket.
			return Arrays.copyOf(entityArray[position[0]][position[1]], count);
		} else {
			// Combine bucket entities and matching overflow entities.
			Entity[] entities = Arrays.copyOf(entityArray[position[0]][position[1]], BUCKET_SIZE);
			Entity[] overflow = getOverflowEntitiesAtPosition(position);
			Entity[] combined = new Entity[entities.length + overflow.length];
			System.arraycopy(entities, 0, combined, 0, entities.length);
			System.arraycopy(overflow, 0, combined, entities.length, overflow.length);
			return combined;
		}
	}
}
