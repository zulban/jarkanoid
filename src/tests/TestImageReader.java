package tests;

import java.util.Arrays;

import agents.*;
import blocks.*;
import simulation.Entity;
import simulation.ImageReader;
import simulation.World;

public class TestImageReader extends ZUnit {
	
	public void testRocks(){
		//All greys/blacks have hit points and are rocks.
		World world=new World(1,1);		
		int[] values={0,1,50,100,150,250,254};
		
		for(int i=0;i<values.length;i++){
			int[] rgb={values[i],values[i],values[i]};
			String rgbText="rgb = ".concat(Arrays.toString(rgb));
			int hp=ImageReader.getBreakableHitPointsFromRGB(rgb);
			
			//have hit points
			assertGreater(hp,0,rgbText);
			
			Entity entity = ImageReader.getEntityFromRGB(world, 0, 0, rgb);
			//are rocks			
			assertTrue(entity instanceof Rock,rgbText);
		}
	}
	
	public void testUnbreakable(){
		World world=new World(1,1);
		assertFalse(world.isSolid(0,0));
		world.putEntityInWorld(new Unbreakable(world,0,0));
		assertTrue(world.isSolid(0,0));
	}
	
}