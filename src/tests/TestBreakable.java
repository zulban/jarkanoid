package tests;

import java.util.Arrays;

import agents.*;
import blocks.*;
import simulation.EntityMap;
import simulation.World;
import simulation.WorldFactory;

public class TestBreakable extends ZUnit {
	
	public void testBasic(){
		World world=new World(1,2);
		Ball ball=new Ball(world,0,0);
		
		//ball starts with correct position
		assertEqual(ball.getPosition(),new int[]{0,0});
		world.putEntityInWorld(ball);		
		//ball keeps its position after placement
		assertEqual(ball.getPosition(),new int[]{0,0});
		
		Rock rock = new Rock(world,0,1,1);
		world.putEntityInWorld(rock);
		//wall is solid
		assertTrue(world.isSolid(0,1));
		world.tick(1000);
		String message=Arrays.toString(ball.getPosition());
		//wall is destroyed
		assertTrue(rock.isDestroyed());
		
		//wall position is not solid
		assertFalse(world.isSolid(0,1),message);
	}
	
	public void testBomb(){
		World world=WorldFactory.createWorldFromString("ob992");
		
		//block is there
		assertTrue(world.isSolid(4,0));
		
		//run turns, but not enough for ball to break walls by itself
		world.tick(20);
		
		//block is now broken by bomb through the wall
		assertFalse(world.isSolid(4,0));		
	}
}