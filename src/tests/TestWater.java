package tests;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;

import agents.*;
import blocks.*;
import console.ConsoleColor;
import console.ConsoleView;
import generic.Logging;
import generic.Utilities;
import simulation.World;
import simulation.WorldFactory;

public class TestWater extends ZUnit {

	public void testWaterAtPosition() {
		World world = new World(1, 1);
		assertFalse(world.isPositionWater(0, 0));
		world.putEntityInWorld(new Water(world, 0, 0));
		assertTrue(world.isPositionWater(0, 0));
	}
	
	public void testCountWater(){
		World world = WorldFactory.createWorldFromString("--------/---wwww--/---wwww--/--------/--------");
		
		int result = world.countWaterNearPoint(4, 3, 8, 3);
		assertEqual(result,8);
		
		result = world.countWaterNearPoint(0, 0, 8, 2);
		assertEqual(result,0);
	}

	public void testPermeable() {
		World world = WorldFactory.createWorldFromString("w-/--");
		// outside world is not permeable
		assertFalse(world.isWaterPermeable(-1, 1));
		// water position is not permeable
		assertFalse(world.isWaterPermeable(0, 1));
		// air is permeable
		assertTrue(world.isWaterPermeable(1, 1));
	}

	public void testCanBlobMove() {
		// w -
		// w -

		World world = new World(2, 2);
		world.putEntityInWorld(new Water(world, 0, 0));
		Water water = new Water(world, 0, 1);
		world.putEntityInWorld(water);

		// top water can blob move (bottom water is a "row" of waters).
		assertTrue(water.canBlobMove());
	}
	
	public void testHasMovedThisTick(){
		World world = new World(3,3);
		Water water = new Water(world,0,2);
		world.putEntityInWorld(water);
		assertFalse(water.hasMovedThisTick());
		world.tick();
		assertTrue(water.hasMovedThisTick());
		world.tick();
		assertTrue(water.hasMovedThisTick());
	}
	
	public void testBlobMovePositions(){
		World world = WorldFactory.createWorldFromString("------/wwwww-");
		Water water = new Water(world,0,1);
		world.putEntityInWorld(water);
		Logging.setWriteToConsole(true);
		ArrayList<int[]> points = (ArrayList<int[]>) water.getBlobMovePositions();
		Logging.setWriteToConsole(false);
		
		assertEqual(points.size(),1,ConsoleView.getWorldString(world));
		if(points.size()==1){
			int[] point = points.get(0);
			assertEqual(point,new int[]{5,0});
		}
	}

	public void testBlobMove() {
		// w = water e = expected

		// w - -
		// w w e
		World world = WorldFactory.createWorldFromString("w--/ww-");
		world.tick();

		for (int i = 0; i < 3; i++)
			assertEqual(world.getWaterDepth(i, 0), 0, ConsoleView.getWorldString(world)+("\ni = " + i));

		// bottom level is filled with water, no water on top row
		for (int i = 1; i < world.getWidth(); i++) {
			assertTrue(world.isWaterAtPosition(i, 0), "(" + i + "," + "0)");
			assertFalse(world.isWaterAtPosition(i, 1), "(" + i + "," + "0)");
		}
	}

	public void testConservationOfWaterSimple() {
		// w - w
		// w w -

		World world = WorldFactory.createWorldFromString("w-w/ww-");

		assertEqual(countWaters(world), 4);
		//Logging.setWriteToConsole(true);
		world.tick();
		//Logging.setWriteToConsole(false);
		assertEqual(countWaters(world), 4);
	}

	public void testConservationOfWater() {
		// Add n waters, tick world many times, still has exactly n waters.
		String w = "--wwwwwwww/" + "ww--ww--ww/" + "---wwww---/" + "w--------w/" + "wwww-----w/" + "ww------ww/"
				+ "-w-w-w-w-w/" + "w-w-w-w-w-";
		World world = WorldFactory.createWorldFromString(w);
		int expected = Utilities.countOccurencesInString(w, "w");

		int result = countWaters(world);
		assertEqual(result, expected);

		world.tick(50);
		result = countWaters(world);
		assertEqual(result, expected);
	}

	private int countWaters(World world) {
		int count = 0;
		for (int i = 0; i < world.getWidth(); i++) {
			for (int j = 0; j < world.getHeight(); j++) {
				if (world.isWaterAtPosition(i, j))
					count++;
			}
		}
		return count;
	}

	public void testTumble() {
		// w = water e = expected

		// w -
		// w e
		World world = WorldFactory.createWorldFromString("w-/w-");

		world.tick();
		assertTrue(world.isWaterAtPosition(0, 0));
		assertTrue(world.isWaterAtPosition(1, 0));
		assertFalse(world.isWaterAtPosition(0, 1));
		assertFalse(world.isWaterAtPosition(1, 1));
	}
}