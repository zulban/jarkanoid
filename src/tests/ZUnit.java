package tests;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import console.ConsoleColor;
import generic.Utilities;

public class ZUnit {
	private int testCount, successCount, failCount, errorCount;
	private static int totalTestCount, totalSuccessCount, totalFailCount, totalErrorCount;
	private static ArrayList<String> failures = new ArrayList<String>();

	public <T> void assertGreater(Comparable<T> expectedGreater, T expectedLesser) {
		assertGreater(expectedGreater, expectedLesser, "");
	}

	public <T> void assertGreater(Comparable<T> expectedGreater, T expectedLesser, String message) {
		if (expectedGreater.compareTo(expectedLesser) == 1) {
			logSuccess();
		} else {
			logFailure(
					"assertGreater failed. "+ message+"\nNot true: " + expectedGreater.toString() + " > " + expectedLesser );
		}
	}

	public void assertFalse(boolean value, String message) {
		if (!value) {
			logSuccess();
		} else {
			logFailure("assertFalse failed. " + message);
		}
	}

	public void assertFalse(boolean value) {
		assertFalse(value, "");
	}

	public void assertTrue(boolean value, String message) {
		if (value) {
			logSuccess();
		} else {
			logFailure("assertTrue failed. " + message);
		}
	}

	public void assertTrue(boolean value) {
		assertTrue(value, "");
	}
	
	public void assertEqual(int[] array1, int[] array2) {
		assertEqual(array1,array2,"");
	}

	public void assertEqual(int[] array1, int[] array2, String message) {
		if (Utilities.compareArrays(array1, array2)) {
			logSuccess();
		} else {
			logFailure("assertEqual " + Arrays.toString(array1) + " != " + Arrays.toString(array2)+". "+message);
		}
	}
	
	public <T> void assertEqual(Comparable<T> item1, T item2) {
		assertEqual(item1,item2,"");
	}
	
	public <T> void assertEqual(Comparable<T> item1, T item2, String message) {
		if (item1.compareTo(item2)==0) {
			logSuccess();
		} else {
			logFailure("assertEqual " + item1 + " != " + item2+". "+message);
		}
	}

	private void logSuccess() {
		successCount++;
		testCount++;
	}
	
	private String getStackTraceInfoToString(StackTraceElement[] ste, String message, boolean fullStackTrace){
		
		String trace = "    Full stack trace:";
		String lineNumber = "";
		for (int i = 0; i < ste.length; i++) {
			if (lineNumber.length() == 0 && ste[i].toString().startsWith(this.getClass().getName()))
				lineNumber = ste[i].toString();
			trace = trace.concat("\n" + ste[i].toString());
		}
		
		return "Class: "+this.getClass().getName() 
				+ (message.length()>0?"\n" + message:"") + "\n" 
		+ lineNumber 
		+ (fullStackTrace?"\n\n" + trace:"");
	}

	private void logFailure(String assertInfo) {
		failCount++;
		testCount++;
		StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		String message=getStackTraceInfoToString(ste,assertInfo,false);
		failures.add(message);
	}
	
	private void logError(Exception e,Method method) {
		errorCount++;
		testCount++;
		StackTraceElement[] ste = e.getStackTrace();
		String message=getStackTraceInfoToString(ste,method.getName(),true);
		failures.add(message+"\n\n");
	}

	public void runAllTests() {
		ConsoleColor.printOrange("Starting: " + this.getClass().getName());
		Pattern pattern = Pattern.compile("test.+");
		for (Method method : this.getClass().getMethods()) {
			if (pattern.matcher(method.getName()).matches()) {
				try {
					method.invoke(this);
				} catch (Exception e) {
					logError(e,method);
				}
			}
		}
		ConsoleColor.printOrange("\nRan " + testCount + " tests for: " + this.getClass().getName());
		ConsoleColor.printGreen(successCount + " successes.");
		if (failCount > 0)
			ConsoleColor.printRed(failCount + " failures.");
		if (errorCount > 0)
			ConsoleColor.printRed(errorCount + " errors.");

		totalTestCount += testCount;
		totalFailCount += failCount;
		totalErrorCount += errorCount;
		totalSuccessCount += successCount;
	}

	public static void showFinalReport() {
		for (int i = 0; i < failures.size(); i++) {
			String message = failures.get(i);
			ConsoleColor.printRed("\n*********************************");
			ConsoleColor.printRed("Failure " + (i + 1) + " of " + failures.size() + ":");
			System.out.println("\n" + message);
		}
		ConsoleColor.printYellow("\nFinished all tests. Ran " + totalTestCount + " tests.");
		ConsoleColor.printGreen(totalSuccessCount + " successes.");
		if (totalFailCount > 0)
			ConsoleColor.printRed(totalFailCount + " failures.");
		if (totalErrorCount > 0)
			ConsoleColor.printRed(totalErrorCount + " errors.");
	}
}
