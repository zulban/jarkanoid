package tests;

import java.util.LinkedList;
import java.util.List;

public class TestRunner {
	public static void main(String[] args){
		(new TestWater()).runAllTests();
		(new TestBall()).runAllTests();
		(new TestImageReader()).runAllTests();
		(new TestBreakable()).runAllTests();
		
		ZUnit.showFinalReport();
	}
}
