package tests;

import java.util.Arrays;

import agents.*;
import blocks.*;
import simulation.EntityMap;
import simulation.World;

public class TestBall extends ZUnit {
	
	public void testBallsInEntityMap(){
		World world=new World(1,2);
		int ballCount=EntityMap.BUCKET_SIZE+1;
		for(int i=0;i<ballCount;i++)
			world.putEntityInWorld(new Ball(world,0,0));
		int result=world.countEntitiesInWorld();
		assertEqual(result,ballCount);
		world.tick(20);
		assertEqual(result,ballCount);
		
		int size=10;
		world=new World(size,size);
		for(int i=0;i<size;i++)
			world.putEntityInWorld(new Ball(world,i,0));
		world.tick(10);
		
		result=world.countEntitiesInWorld();
		assertEqual(result,size);
	}
}